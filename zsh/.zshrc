# start / attach to a tmux session if this is an ssh login
if [[ $- == *i* ]] && [[ -z "$TMUX" ]] && [[ -n "$SSH_CONNECTION" ]] && [[ ! "$TERM" = "dumb" ]] && hash tmux 2>/dev/null; then
  tmux -u new-session -A -s ssh_tmux
fi

# welcome stuff, disabled for dumb shells:
if [[ ! "$TERM" = "dumb" ]]; then
    # only try neofetch if it's avaialbe
    if hash neofetch 2>/dev/null; then
        neofetch
    fi
    # kim jong un title
    echo "Welcome $(whoami), $(shuf -n 1 ~/dotfiles/titles.txt)."
fi

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
# correction:
setopt correct
setopt correctall
setopt autocd
setopt cdablevars

autoload -U colors && colors

# completion stuff
source $HOME/.zsh/completion.zsh
fpath+=$HOME/.zsh/zsh-completions/src
autoload -Uz compinit
compinit
# auto suggestions
source $HOME/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# key bindings
bindkey "e[1~" beginning-of-line
bindkey "e[4~" end-of-line
bindkey "e[5~" beginning-of-history
bindkey "e[6~" end-of-history
bindkey "e[3~" delete-char
bindkey "e[2~" quoted-insert
bindkey "e[5C" forward-word
bindkey "eOc" emacs-forward-word
bindkey "e[5D" backward-word
bindkey "eOd" emacs-backward-word
bindkey "ee[C" forward-word
bindkey "ee[D" backward-word
bindkey "^H" backward-delete-word
bindkey "^[[A" history-beginning-search-backward
bindkey "^[[B" history-beginning-search-forward
# for rxvt
bindkey "e[8~" end-of-line
bindkey "e[7~" beginning-of-line
# for non RH/Debian xterm, can't hurt for RH/DEbian xterm
bindkey "eOH" beginning-of-line
bindkey "eOF" end-of-line
# for freebsd console
bindkey "e[H" beginning-of-line
bindkey "e[F" end-of-line
# completion in the middle of a line
bindkey '^i' expand-or-complete-prefix

# exa / ls
if hash exa 2>/dev/null; then
    alias ls="exa --header --time-style=long-iso"
else
    alias ls="ls --color=auto"
fi
if hash safe-rm 2>/dev/null; then
    alias rm="safe-rm"
fi
alias ll="ls -lh"
alias la="ll -a"
alias -g gr='| rg -i'
alias sudo='sudo '
alias xt="export TERM=xterm-256color"

# go
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

# user paths
export PATH=$PATH:~/.local/bin
export PATH=$PATH:~/.cargo/bin
export PATH=/opt/mxe/usr/bin:$PATH

alias em="emacsclient"
alias emn="em -nw"

export PAGER=less
export EDITOR=mg
export LANG=en_GB.UTF-8
export LC_MESSAGES="C"
export TIME_STYLE=long-iso

export AWS_PROFILE=wasabi

if [ -f ~/.proxy ]; then
    source ~/.proxy
fi

if [[ "$TERM" == "dumb" ]]; then
    unsetopt zle
    unsetopt prompt_cr
    unsetopt prompt_subst
    unfunction precmd
    unfunction preexec
    PS1='$ '
else
    source ~/.zsh/powerlevel10k/powerlevel10k.zsh-theme
    # To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
    [[ ! -f ~/.zsh/.p10k.zsh ]] || source ~/.zsh/.p10k.zsh
fi

